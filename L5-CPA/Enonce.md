# CPA sur AES

Vous disposez d’un AES programmé en langage C dans le dossier [aes_encrypt](../aes_encrypt/). Celui-ci est la cible de ce TP.

Le but est de vous sensibiliser à la cryptanalyse physique. Plus précisément, vous devez réaliser une attaque de type Correlation Power Analysis (CPA) sur le premier tour de l’AES. L’attaque consiste à retrouver la clé de chiffrement de votre AES.

**Toutes les commandes bash sont à exécuter dans un terminal Nix**

## Mesures

**Q1)** Quel est le modèle d’attaquant de cette attaque ? (Quelles capacités, quelle cible, ...)

Compilez votre AES à l’aide de `make` dans [aes_encrypt](../aes_encrypt/). Les fichiers générés se trouvent dans le répertoire *bin*.

**Q2)** Ouvrez le fichier [aes.list](../aes_encrypt/bin/aes.list) et comparez le code source de l’AES dans [aes.c](../aes_encrypt/src/aes.c). Que contient [aes.list](../aes_encrypt/bin/aes.list) ?

Vérifiez que votre application fonctionne sur le microcontrôleur. Depuis le répertoire [aes_encrypt](../aes_encrypt/), exécutez le script de test : 
```bash
python3 ../test/J-test-aes.py .
```

Ce script calcule l’AES à la fois sur votre microcontrôleur et sur le PC hôte, puis calcule la différence. L’application fonctionne-t-elle ?

Nous pouvons maintenant lancer les mesures de consommation de courant.
- Lancez les mesures à l’aide du script correspondant
```bash
python3 ../L5-CPA/M-CPA.py [cible] [nombre d’exécutions] [symbole]
```
- La cible est le répertoire de l’application, ici [aes_encrypt](../aes_encrypt/).
- Le nombre d’exécutions est le nombre de fois où nous exécutons un chiffrement AES dont on mesure la consommation.
- Trouvez le nom du symbole (i.e., de la fonction) dont vous voulez la consommation de courant, dans le fichier [aes.c](../aes_encrypt/src/aes.c).

Les fichiers [plaintexts.npy](../aes_encrypt/plaintexts.npy), [ciphertexts.npy](../aes_encrypt/ciphertexts.npy) et [traces.npy](../aes_encrypt/traces.npy) sont alors générés, au format *npy* de numpy.

**Q3)** Expliquez dans le compte rendu ce que vous avez mesuré et pourquoi. Ajoutez une visualisation d’une trace.

## Prédictions

Pour charger les mesures dans votre script d’analyse, vous avez besoin des librairies *numpy* pour Python ou *NPZ* pour Julia.

**Q4)** Fixez-vous un octet de la clé du premier tour (par exemple, prenez uniquement le premier octet), combien y a-t-il de valeurs possibles pour cet octet de clé ?

**Q5)** Rappelez le chemin d’attaque (quels sont vos observables) et votre modèle.

Pour l’octet que vous avez choisi, construisez sa matrice de prédiction **P**.

**Q6)** Quelles sont les dimensions de cette matrice **P** et pourquoi ?

**Q7)** Les matrices de traces et de prédictions ont-elles les mêmes dimensions ?

## Confrontation

**Q8)** Rappelez ce qu’est un distingueur. Quel distingueur choisissez-vous ici et pourquoi ?

Pour l’octet que vous attaquez, confrontez les mesures avec sa matrice de prédiction à l’aide du distingueur choisi.

**Q9)** Tracez les différentes courbes de corrélation pour toutes les hypothèses.

**Q10)** Quelle est la valeur de l’octet de clé ciblé ? Est-ce la bonne valeur, sinon quel rang a la bonne valeur ?

## Attaque complète

Répétez l’attaque pour les autres octets de clé.

**Q11)** Tracez les corrélations correspondantes.

**Q12)** Retrouvez-vous tous les octets ? Sinon, proposez un moyen efficace de les retrouver.

**Q13)** Identifiez à quel instant sont manipulés les différents octets de la clé ? Reliez ces instants à des instructions dans le fichier [aes.list](../aes_encrypt/bin/aes.list).

**Q14)** De combien de courbes avez-vous besoin au minimum pour que l’attaque fonctionne ?

**Q15)** Rédigez une conclusion du TP, mentionnez les forces et faiblesses de cette attaque.
